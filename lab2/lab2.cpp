// lab2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "lab2.h"


// Global Variables:
HINSTANCE hInst;								// current instance

HWND hMainDlg;
HWND hNewItemDlg;
HICON hIcon;
INT_PTR CALLBACK	MainDlg(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	NewItemDlg(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;

	// Perform application initialization:
	hIcon =(HICON) LoadImage(hInstance, MAKEINTRESOURCE(IDI_LAB2), IMAGE_ICON, 32, 32, LR_SHARED);
	INITCOMMONCONTROLSEX icce;
	icce.dwSize = sizeof(icce);
	icce.dwICC = ICC_LISTVIEW_CLASSES;

	InitCommonControlsEx(&icce);


	hMainDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_MAIN_DLG), NULL, MainDlg);
	hNewItemDlg = CreateDialog(hInst, MAKEINTRESOURCE(IDD_NEW_ITEM), hMainDlg, NewItemDlg);
	ShowWindow(hMainDlg, nCmdShow);
	UpdateWindow(hMainDlg);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!IsDialogMessage(hMainDlg, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int) msg.wParam;
}


INT_PTR CALLBACK MainDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	int result = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		{
			HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);

			HIMAGELIST hLarge;
			hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON),
									  GetSystemMetrics(SM_CYICON),
									  ILC_MASK, 1, 1);

			SendMessage(hDlg, WM_SETICON, ICON_BIG, reinterpret_cast<LPARAM>(hIcon));
			SendMessage(hDlg, WM_SETICON, ICON_SMALL, reinterpret_cast<LPARAM>(hIcon));

			LVCOLUMN column;
			ZeroMemory(&column, sizeof(column));
			column.mask = LVCF_TEXT | LVCF_WIDTH;
			column.cx = COLUMN_WIDTH;
			column.pszText = TEXT(COLUMN_TITLE);

			ListView_InsertColumn(hListView, 1, &column);
			ImageList_AddIcon(hLarge, hIcon);
			SendMessage(GetDlgItem(hDlg, IDC_LIST), BM_SETCHECK, BST_CHECKED, 0);
			ListView_SetImageList(hListView, hLarge, LVSIL_NORMAL);
			ListView_SetImageList(hListView, hLarge, LVSIL_SMALL);
			
		}
		return (INT_PTR)TRUE;
	case WM_CLOSE:
		DestroyWindow(hDlg);
		return (INT_PTR)TRUE;
	case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDOK:
				{
					 DestroyWindow(hDlg);
					 return (INT_PTR)TRUE;
				}
				case IDC_LIST:
				{
					HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);
					ListView_SetView(hListView, LV_VIEW_LIST);
				}
				break;
				case IDC_DETAILS:
				{
					HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);
					ListView_SetView(hListView, LV_VIEW_DETAILS);
				}
				break;
				case IDC_TILE:
				{
					HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);
					ListView_SetView(hListView, LV_VIEW_TILE);
				}
				break;
				case IDC_ICON:
				{
					HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);
					ListView_SetView(hListView, LV_VIEW_ICON);
				}
				break;
				case IDC_ADD:
				{
					DialogBox(hInst, MAKEINTRESOURCE(IDD_NEW_ITEM), hDlg, NewItemDlg);				
				}
				break;
				case IDC_EDIT_ITEM:
				{
					HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);
					int current = ListView_GetNextItem(hListView, -1, LVNI_SELECTED);
					if (current >= 0)
						DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_NEW_ITEM), hDlg, NewItemDlg, (LPARAM)&current);				
				}
				break;
				case IDC_DELETE_ITEM:
				{
					HWND hListView = GetDlgItem(hDlg, IDC_LIST_VIEW);
					int current = ListView_GetNextItem(hListView, -1, LVNI_SELECTED);
					if (current >= 0)
						ListView_DeleteItem(hListView, current);
				}
				break;

			}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (INT_PTR)FALSE;
}

LVITEM CreateNewItem(TCHAR * text)
{
	LVITEM newItem;
	ZeroMemory(&newItem, sizeof(newItem));
	newItem.mask = LVIF_TEXT | LVCF_IMAGE;
	newItem.cchTextMax = 255;
	newItem.pszText = text;
	newItem.iImage = 0;
	return newItem;
}


INT_PTR CALLBACK NewItemDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	static bool isEdit = false;
	static int current = -1;
	switch (message)
	{
	case WM_INITDIALOG:

		SendMessage(hDlg, WM_SETICON, ICON_BIG, reinterpret_cast<LPARAM>(hIcon));
		SendMessage(hDlg, WM_SETICON, ICON_SMALL, reinterpret_cast<LPARAM>(hIcon));

		if (lParam)
		{
			TCHAR buff[255];
			HWND hListView = GetDlgItem(hMainDlg, IDC_LIST_VIEW);

			SendMessage(hDlg, WM_SETTEXT, NULL, (LPARAM)_T("Edit item"));

			current = *(int*)lParam;
			ListView_GetItemText(hListView, current, 0, buff, 255);
			SetDlgItemText(hDlg, IDC_EDIT1, buff);
			isEdit = true;
		}
		return (INT_PTR)TRUE;
	case WM_CLOSE:
		EndDialog(hDlg, LOWORD(wParam));
		return (INT_PTR)TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
			case IDOK:
			{
				HWND hListView = GetDlgItem(hMainDlg, IDC_LIST_VIEW);
				TCHAR buff[255];
				GetDlgItemText(hDlg, IDC_EDIT1, buff, 255);
				SetDlgItemText(hDlg, IDC_EDIT1, NULL);
				if (isEdit)
				{
					ListView_SetItemText(hListView, current, 0, buff);
					isEdit = false;
				}
				else
					ListView_InsertItem(hListView, &CreateNewItem(buff));
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			}
			break;
			case IDCANCEL:
				EndDialog(hDlg, LOWORD(wParam));
				return (INT_PTR)TRUE;
			break;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
