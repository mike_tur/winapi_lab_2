//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by lab2.rc
//
#define IDC_MYICON                      2
#define IDD_LAB2_DIALOG                 102
#define IDS_APP_TITLE                   103
#define IDD_MAIN_DLG                    103
#define IDI_LAB2                        107
#define IDI_SMALL                       108
#define IDC_LAB2                        109
#define IDR_MAINFRAME                   128
#define IDD_NEW_ITEM                    129
#define IDC_LIST_VIEW                   1001
#define IDC_LIST                        1006
#define IDC_DETAILS                     1007
#define IDC_TILE                        1008
#define IDC_ICON                        1009
#define IDC_ADD                         1010
#define IDC_EDIT1                       1011
#define IDC_BUTTON1                     1012
#define IDC_EDIT_ITEM                   1012
#define IDC_BUTTON2                     1014
#define IDC_DELETE_ITEM                 1014
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
